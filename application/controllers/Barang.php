<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Barang extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		// load model terkait
		$this->load->model("barang_model");
		$this->load->model("jenis_barang_model");
	}
	
	public function index()
	{
		$this->listbarang();
	}
	
	public function listbarang()
	{
		$data['data_jenis_barang'] = $this->jenis_barang_model->tampilDataJenisBarang();
		$data['data_barang'] = $this->barang_model->tampilDataBarang2();
		$this->load->view('home_barang', $data);
	}
	
	public function input()
	{
		$data['data_jenis_barang'] = $this->jenis_barang_model->tampilDataJenisBarang();
		
		if (!empty($_REQUEST)) {
			$m_barang = $this->barang_model;
			$m_barang->save();
			redirect("barang/index", "refresh");
		}
		$this->load->view('input_barang', $data);
	}
	
	public function detailbarang($kode_barang)
	{
		$data['detail_barang'] = $this->barang_model->detail($kode_barang);
		$this->load->view('Detail_barang', $data);
	}
	
	public function edit($kode_barang)
	{
		$data['data_jenis_barang'] = $this->jenis_barang_model->tampilDataJenisBarang();
		$data['detail_barang'] = $this->barang_model->detail($kode_barang);
		
		if (!empty($_REQUEST)) {
			$m_barang = $this->barang_model;
			$m_barang->update($kode_barang);
			redirect("barang/index", "refresh");
		}
		$this->load->view('edit_barang', $data);
	}
	
	public function delete($kode_barang)
	{
		$m_barang = $this->barang_model;
		$m_barang->delete($kode_barang);
		redirect("barang/index", "refresh");
	}
	
}